; example.asm
comment ^

        This sample code is designed to show how one might implement an
        NPU function on the 80x87 class of Numeric Processing Units.

        written on Fri  09-01-1995  by Ed Beroset
        and released to the public domain by the author

^
        .MODEL small
        .486


        .STACK   400h

        .data
OneEighty   dw  180                     ; handy const for conversion
angle       dq  30.0                    ; our angle (in degrees)
result      dq  ?                       ;

        .code
main proc
        .startup
        finit                           ; initialize the NPU
        fldpi                           ; ST(0) = pi
        fld     [angle]                 ; ST(0) = angle, ST(1) = pi
        fmulp   st(1),st                ; ST(0) = angle * pi
        fild    [OneEighty]             ; ST(0) = 180, ST(1) = angle * pi
        fdivp   st(1),st                ; ST(0) = angle (now in radians)
        fsin                            ; ST(0) = sin(angle)
        fstp    [result]                ; store sin(angle) in result
        .exit 0
main endp
        END
