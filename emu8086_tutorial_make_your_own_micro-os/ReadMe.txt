======================================
   making your own operating system
======================================

source files:
   micro-os_loader.asm
   micro-os_kernel.asm
 
utility:
   writebin.asm
  
header file (can be used to add additional features to the os)
   emu8086.inc
   
documentation:
   tutorial.html   - the tutorial file.  
   8086_bios_and_dos_interrupts.html - basic interrupt list.

embedded images: 
   img/write_bin.gif
   img/floppy.gif
   img/write_bin_menu.png
   img/white_boot_sector.png
   img/back_to_top.gif

======================================

you can compile source files using any emu8086 compatible assembler,
or you can download emu8086 from here:

http://www.emu8086.com

feel free to email if you have any questions: info@emu8086.com

======================================

we hope you enjoy this tiny tutorial.



emu8086.com (c) 2005

all rights reserved.
