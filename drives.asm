; DRIVES.ASM
;
; this simple program prints all valid logical drive letters under
; DOS 3.0 and higher.
;
; NOTES:
;   1.  Technically, one could have drives like ]: but these are
;       typically not used and are ignored by this program.
;   2.  Under an OS/2 DOS box, attempting to read drive A or drive B
;       will cause a popup box to appear if there is no diskette
;       currently inserted.  The simplest way around this is to
;       change it so that the first drive letter is "C:"
;
; written on Thu  01-02-1997  by Edward J. Beroset
;       and released to the public domain by the author
;
        .model tiny
        .code
        org 100h
Start proc
        mov si,offset DriveLtr          ; DS:SI ==> ASCIIZ path
        mov di,offset DummyBuff         ; ES:DI ==> 128 byte buffer
        mov ah,60h                      ; use "TRUENAME" DOS function
        int 21h                         ;
        mov dl,[DriveLtr]               ; put drive letter in DL
        jc  Skip                        ; skip printing for invalid drives
        mov ah,02h                      ; print using write char func 02h
        int 21h                         ;
Skip:                                   ;
        inc dl                          ; increment drive letter
        mov [DriveLtr],dl               ; put it back into place
        cmp dl,'Z'                      ; search only A: through Z:
        jbe Start                       ; keep going if we're not at Z
        mov ah,4Ch                      ; exit to DOS
        int 21h                         ;
Start endp

        .data
DriveLtr  db "C:\.",0                   ; drive letter
        .data?
DummyBuff db 128 DUP (?)                ; buffer req'd by DOS but unused here
        END Start
