TITLE "BootThru.Asm" v1.1
;
; written on Fri  02-11-1994  by Ed Beroset
; released to the public domain by the author
;
; copies the MBR from the hard drive indicated by TARGETDRIVE, modifies it
; for boot from floppy, and copies to boot record of floppy.  This is SAMPLE
; CODE only, and not meant as a fully developed utility. As written, this
; should boot the first hard drive from A:
;
; *** USE THIS CODE AT YOUR OWN RISK! ***

TARGETDRIVE     equ       0             ; hard drive (0, 1, ...)

cseg segment
assume cs:cseg
org 100h                                ; .COM format

Start:
        mov     dx,80h + TARGETDRIVE    ; read MBR of target drive
        mov     cx,1h                   ; it's always the first sector
        mov     bx, OFFSET buffer       ; point to our buffer
        mov     ax, 0201h               ; read one sector
        int     13h
        mov     cx,4h                   ; read up to four entries
        mov     si, OFFSET buffer + 1BEh; scan our partition table
find_bootable:
        test    byte ptr[si],80h        ; load it up
        jz      skip_it                 ; if it's not bootable, skip it
        add     byte ptr[si],TARGETDRIVE; add target drive
skip_it:
        add     si,10h                  ; v1.1 bugfix
        loop    find_bootable
        xor     dx,dx                   ; zero dx (drive A: head 0)
        mov     cx,1h                   ; first sector (v1.1 clarity fix)
        mov     ax,0301h                ; bx still points to buffer
        int     13h                     ; write to floppy in A:

        mov ax, 4C00h                   ; Exit to DOS.
        int 21h

buffer db 0                             ;Dynamic buffer, 512 bytes.

cseg ends
end Start
